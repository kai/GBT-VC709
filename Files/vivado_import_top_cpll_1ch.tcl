#
#	File import script for the VC709 GBT project
#
#

#Script Configuration

set proj_name bnl_gbt_vc709_cpll_1ch
# Set the supportfiles directory path
set scriptdir [pwd]
set firmware_dir $scriptdir
# Vivado project directory:
set project_dir $firmware_dir/Projects/$proj_name

#Close currently open project and create a new one. (OVERWRITES PROJECT!!)
close_project -quiet
set PART xc7vx690tffg1761-2
create_project -force -part xc7vx690tffg1761-2 $proj_name $firmware_dir/Projects/$proj_name

set_property target_language VHDL [current_project]
set_property default_lib work [current_project]



import_ip $firmware_dir/Sources/IP/clk_wiz_3.xcix
import_ip $firmware_dir/Sources/IP/clk_wiz_4.xcix
import_ip $firmware_dir/Sources/IP/clk_wiz_6.xcix
import_ip $firmware_dir/Sources/IP/clk_wiz_7.xci
import_ip $firmware_dir/Sources/IP/vio_1.xci


#read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/FELIX_gbt_wrapper.vhd
read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/FELIX_GBT_RXSLIDE_FSM.vhd
read_vhdl -library work $firmware_dir/Sources/GT_CPLL_Wrapper.vhd
read_vhdl -library work $firmware_dir/Sources/GBT_Wrapper.vhd
read_vhdl -library work $firmware_dir/Sources/GBT/FELIX_gbt_package.vhd
read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/FELIX_GBT_RX_AUTO_RST.vhd
read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/gbt_rx_decoder_FELIX.vhd
read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/gbt_rx_decoder_gbtframe_chnsrch.vhd
read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/gbt_rx_decoder_gbtframe_deintlver.vhd
read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/gbt_rx_decoder_gbtframe_elpeval.vhd
read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/gbt_rx_decoder_gbtframe_errlcpoly.vhd
read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/gbt_rx_decoder_gbtframe_lmbddet.vhd
read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/gbt_rx_decoder_gbtframe_rs2errcor.vhd
read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/gbt_rx_decoder_gbtframe_rsdec.vhd
read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/gbt_rx_decoder_gbtframe_rsdec_sync.vhd
read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/gbt_rx_decoder_gbtframe_syndrom.vhd
read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/gbt_rx_descrambler_16bit.vhd
read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/gbt_rx_descrambler_21bit.vhd
read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/gbt_rx_descrambler_FELIX.vhd
read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/gbt_rx_gearbox_FELIX.vhd

read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/gbt_tx_encoder_FELIX.vhd
read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/gbt_tx_encoder_gbtframe_polydiv.vhd
read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/gbt_tx_encoder_gbtframe_intlver.vhd
read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/gbt_tx_encoder_gbtframe_rsencode.vhd
read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/gbt_tx_gearbox_FELIX.vhd
read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/gbt_tx_scrambler_16bit.vhd
read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/gbt_tx_scrambler_21bit.vhd
read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/gbt_tx_scrambler_FELIX.vhd
read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/gbt_tx_timedomaincrossing_FELIX.vhd

read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/gbtRx_FELIX.vhd
read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/gbtTx_FELIX.vhd
read_vhdl -library work $firmware_dir/Sources/GBT/gbt_code/gbtTxRx_FELIX.vhd

read_vhdl -library work $firmware_dir/Sources/gtwizard_CPLL_4p8g_V7_2016p1/gtwizard_cpll_4p8g_v7_2016p1.vhd
read_vhdl -library work $firmware_dir/Sources/gtwizard_CPLL_4p8g_V7_2016p1/gtwizard_cpll_4p8g_v7_2016p1_auto_phase_align.vhd
read_vhdl -library work $firmware_dir/Sources/gtwizard_CPLL_4p8g_V7_2016p1/gtwizard_cpll_4p8g_v7_2016p1_gt.vhd
read_vhdl -library work $firmware_dir/Sources/gtwizard_CPLL_4p8g_V7_2016p1/gtwizard_cpll_4p8g_v7_2016p1_init.vhd
read_vhdl -library work $firmware_dir/Sources/gtwizard_CPLL_4p8g_V7_2016p1/gtwizard_cpll_4p8g_v7_2016p1_multi_gt.vhd
read_vhdl -library work $firmware_dir/Sources/gtwizard_CPLL_4p8g_V7_2016p1/gtwizard_cpll_4p8g_v7_2016p1_rx_startup_fsm.vhd
read_vhdl -library work $firmware_dir/Sources/gtwizard_CPLL_4p8g_V7_2016p1/gtwizard_cpll_4p8g_v7_2016p1_tx_startup_fsm.vhd
read_vhdl -library work $firmware_dir/Sources/gtwizard_CPLL_4p8g_V7_2016p1/gtwizard_cpll_4p8g_v7_2016p1_sync_block.vhd
read_vhdl -library work $firmware_dir/Sources/gtwizard_CPLL_4p8g_V7_2016p1/gtwizard_cpll_4p8g_v7_2016p1_sync_pulse.vhd
read_vhdl -library work $firmware_dir/Sources/gtwizard_CPLL_4p8g_V7_2016p1/gtwizard_cpll_4p8g_v7_2016p1_tx_manual_phase_align.vhd
read_vhdl -library work $firmware_dir/Sources/gtwizard_CPLL_4p8g_V7_2016p1/gtwizard_cpll_4p8g_v7_2016p1_cpll_railing.vhd

# ----------------------------------------------------------
# Update IP to latest Vivado version
# ----------------------------------------------------------

upgrade_ip [get_ips  {vio_1 clk_wiz_3 clk_wiz_4 clk_wiz_6 clk_wiz_7}]

# ----------------------------------------------------------
# XDC constraints files
# ----------------------------------------------------------

read_xdc -verbose $firmware_dir/Sources/top.xdc



puts "INFO: Done!"
