


create_clock -name gtrefclk0_in -period 4.167 [get_pins GTHREFCLK_GEN/O]


create_clock -name GT_TX_WORD_CLK[0] -period 4.167 [get_pins TXUSRCLK_GEN_240.tx_usrclk_bufg1/O]
create_clock -name GT_TX_WORD_CLK[0] -period 4.167 [get_pins TXUSRCLK_GEN_120.tx_usrclk_bufg1/clk_out1]
create_clock -name GT_TX_WORD_CLK[0] -period 4.167 [get_pins TXUSRCLK_GEN_160.tx_usrclk_bufg1/clk_out1]

create_clock -name gt0_rxusrclk -period 4.167 [get_pins rx_usrclk_bufg1/O]


set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/D" } ] 3
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/D" } ] 2

set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*FelixDescrambler/RX_HEADER_O_reg[*]/D" } ] 3
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*FelixDescrambler/RX_HEADER_O_reg[*]/D" } ] 2

set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/feedbackRegister_reg[*]/D" } ] 3
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/feedbackRegister_reg[*]/D" } ] 2

set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*error_buf_reg/D" } ] 3
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*error_buf_reg/D" } ] 2


create_clock -name clk200m -period 5 [get_pins clk200m_i/O]
#set_property SEVERITY {Warning} [get_drc_checks REQP-44]
#set_property SEVERITY {Warning} [get_drc_checks REQP-46]


set_property PACKAGE_PIN AJ32 [get_ports sma_p]
set_property IOSTANDARD LVCMOS18 [get_ports sma_p]
set_property PACKAGE_PIN AK32 [get_ports sma_n]
set_property IOSTANDARD LVCMOS18 [get_ports sma_n]



set_property PACKAGE_PIN H19 [get_ports SYSCLK_P]
set_property IOSTANDARD LVDS [get_ports SYSCLK_P]
set_property PACKAGE_PIN G18 [get_ports SYSCLK_N]
set_property IOSTANDARD LVDS [get_ports SYSCLK_N]



set_property PACKAGE_PIN AN5 [get_ports {gthrxn_in[0]}]

set_property PACKAGE_PIN AM7 [get_ports {gthrxn_in[1]}]

set_property PACKAGE_PIN AL5 [get_ports {gthrxn_in[2]}]

set_property PACKAGE_PIN AJ5 [get_ports {gthrxn_in[3]}]


set_property PACKAGE_PIN AK7 [get_ports mgtrefclk0_x0y8_n]
set_property PACKAGE_PIN AK8 [get_ports mgtrefclk0_x0y8_p]


set_false_path -from [get_clocks clk_out1_clk_wiz_4] -to [get_clocks gt0_rxusrclk]
set_false_path -from [get_clocks clk_out1_clk_wiz_4] -to [get_clocks clk_out1_clk_wiz_3]
set_false_path -from [get_clocks clk_out1_clk_wiz_4] -to [get_clocks GT_TX_WORD_CLK[0]]

set_property PACKAGE_PIN AB41 [get_ports opto_inhibit[0]]
set_property IOSTANDARD LVCMOS18 [get_ports opto_inhibit[0]]

set_property PACKAGE_PIN Y42 [get_ports opto_inhibit[1]]
set_property IOSTANDARD LVCMOS18 [get_ports opto_inhibit[1]]

set_property PACKAGE_PIN AC38 [get_ports opto_inhibit[2]]
set_property IOSTANDARD LVCMOS18 [get_ports opto_inhibit[2]]

set_property PACKAGE_PIN AC40 [get_ports opto_inhibit[3]]
set_property IOSTANDARD LVCMOS18 [get_ports opto_inhibit[3]]
   
 
