----------------------------------------------------------------------------------
-- Company: BNL
-- Engineer: Kai Chen
--
-- Create Date: 02/24/2016 03:55:21 AM
-- Design Name:
-- Module Name: GBT_Wrapper - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
-- Low latency GBT-FPGA example
-- Reference clock can be 120M/240M
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

use work.FELIX_gbt_package.all;

entity GBT_Wrapper is
  Generic
  (
    RefClk : integer   := 240
  );
  Port (
    SYSCLK_P: in std_logic;
    SYSCLK_N: in std_logic;

    gthrxn_in: in std_logic_vector(0 downto 0);
    gthrxp_in: in std_logic_vector(0 downto 0);
    gthtxn_out: out std_logic_vector(0 downto 0);
    gthtxp_out: out std_logic_vector(0 downto 0);

    sma_p  : out std_logic;
    sma_n  : out std_logic;

    opto_inhibit : out std_logic_vector(3 downto 0);
--    io_p  : out std_logic;
--    io_n  : out std_logic;

    mgtrefclk0_x0y8_p : in std_logic;
    mgtrefclk0_x0y8_n : in std_logic
  );
end GBT_Wrapper;

architecture Behavioral of GBT_Wrapper is

component clk_wiz_4 is
  port
  (-- Clock in ports
    clk_in1         : in std_logic;
    clk_out1        : out std_logic;
    reset           : in std_logic;
    locked          : out std_logic
  );
 end component;

component clk_wiz_3 is
  port
  (-- Clock in ports
    clk_in1         : in std_logic;
    clk_out1        : out std_logic;
    reset           : in std_logic;
    locked          : out std_logic
  );
 end component;

 component clk_wiz_6 is
   port
   (-- Clock in ports
     clk_in1         : in std_logic;
     clk_out1        : out std_logic;
     reset           : in std_logic;
     locked          : out std_logic
   );
  end component;

 component clk_wiz_7 is
    port
    (-- Clock in ports
      clk_in1         : in std_logic;
      clk_out1        : out std_logic;
      reset           : in std_logic;
      locked          : out std_logic
    );
   end component;
component vio_1 IS
  PORT (
  clk : IN STD_LOGIC;
  probe_in0 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
  probe_in1 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
  probe_in2 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
  probe_in3 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
  probe_in4 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
  probe_in5 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
  probe_in6 : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
  probe_in7 : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
  probe_in8 : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
  probe_in9 : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
  probe_in10 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
  probe_in11 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
  probe_out0 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  probe_out1 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  probe_out2 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  probe_out3 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  probe_out4 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  probe_out5 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
  probe_out6 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
  probe_out7 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
  probe_out8 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  probe_out9 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  probe_out10 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  probe_out11 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  probe_out12 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  probe_out13 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
  probe_out14 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
  probe_out15 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
  probe_out16 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
  probe_out17 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  probe_out18 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  probe_out19 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  probe_out20 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
  );
  END component;
signal tied_to_ground, tied_to_high,rstt,rx_flag,tx_flag,RX_FLAG_r,SYSTEM_TTC_40M, TC_EDGE: std_logic;

signal mmcm_lck,clk10m, clk200m, clk40m,pulse_lg,io_p_p2,io_p_p1,RX_FRAME_CLK: std_logic;
signal SOFT_RXRST_ALL,SOFT_TXRST_ALL,soft_reset_in, TC_EDGE_vec:std_logic_vector(0 downto 0);
signal TXOUTCLK,gttxreset,SOFT_RXRST_GT,SOFT_TXRST_GT_r,SOFT_TXRST_GT_2r,SOFT_TXRST_GT_3r,SOFT_TXRST_GT,rxoutclk: std_logic_vector(3 downto 0);
signal SOFT_TXRST_GT_4r, SOFT_TXRST_GT_5r, SOFT_TXRST_GT_6r, SOFT_TXRST_GT_7r, SOFT_TXRST_GT_8r, SOFT_TXRST_GT_9r: std_logic_vector(3 downto 0);
signal error_clr_vec,TX_TC_METHOD_vec,rstt_vec:std_logic_vector(0 downto 0);
signal TX_TC_METHOD, gtrefclk0_in, error_clr,gt0_rxusrclk,gt0_rxoutclk_out,clk10m_new:std_logic;
signal tx_tc_dly_value:std_logic_vector(15 downto 0);
signal alignment_chk_rst, alignment_done, rxslide_i, rxslide, DATA_MODE_i, tx_reset_i,rx_reset_i, gt_tx_word_clk,error_fec,errors,userclk_rx_reset_in,userclk_tx_reset_in,TX_RESET_f:std_logic_vector(3 downto 0);

signal gth_tx_rst, gth_rx_rst,gth_cdr_stable,gth_tx_pll_rst, oddeven,gth_rx_pll_rst,gth_tx_rst_done,gth_rx_rst_done,gth_rst,rxfsmresetdone_out,txfsmresetdone_out:std_logic_vector(3 downto 0);
type txrx20b_4ch_type        is array (3 downto 0) of std_logic_vector(19 downto 0);
signal tx_data_20b, rx_data_20b,RX_DATA_20b_i,RX_DATA_20b_r:txrx20b_4ch_type;
type txrx120b_4ch_type        is array (3 downto 0) of std_logic_vector(119 downto 0);
signal RX_120b_out:txrx120b_4ch_type;
type txrx6b_4ch_type        is array (3 downto 0) of std_logic_vector(5 downto 0);
signal errcnt:txrx6b_4ch_type;
type txrx8b_4ch_type        is array (3 downto 0) of std_logic_vector(7 downto 0);
signal txcnt,txcnt_p,txcnt_p1:txrx8b_4ch_type;

signal pulse_cnt:std_logic_vector(29 downto 0);
signal alignment_chk_rst_c1,alignment_chk_rst_c,auto_gbt_rxrst,auto_gth_rxrst,alignment_chk_rst_f,ext_trig_realign,FSM_RST,rxslide_c,gth_rst_f,gth_rx_rst_f,gth_tx_rst_f:std_logic_vector(3 downto 0);

signal   BITSLIP_MANUAL_r, BITSLIP_MANUAL_2r: std_logic_vector(0 downto 0);
signal io_p, io_n: std_logic;

begin

opto_inhibit <= (others => '0');

clk200m_i : IBUFDS
  generic map
  (
	 DIFF_TERM    => TRUE,
	 IBUF_LOW_PWR => FALSE,
	 IOSTANDARD   => "DEFAULT"
  )
  port map
  (
	 I  => SYSCLK_P,
	 IB => SYSCLK_N,
	 O  => clk200m
  );


clk_wiz :  clk_wiz_3
  port map
  (-- Clock in ports
    clk_in1          => GT_TX_WORD_CLK(0),
    clk_out1         => clk40m,
    reset            => '0',--rstt,--'0',
    locked           =>open-- mmcm_lck
  );

clk_wiz2 : clk_wiz_4
  port map
  (-- Clock in ports
    clk_in1          => clk200m,
    clk_out1         => clk10m,
    reset            => '0',--rstt,--'0',
    locked           =>open-- mmcm_lck
  );

 mmcm_lck <='1';





  GTHREFCLK_GEN : IBUFDS_GTE2
  port map (
    I     => mgtrefclk0_x0y8_p,
    IB    => mgtrefclk0_x0y8_n,
    CEB   => '0',
    O     => gtrefclk0_in,
    ODIV2 => open
  );



 process(clk40m)
  begin
    if clk40m'event and clk40m='1' then
      pulse_lg <= pulse_cnt(20);
      if pulse_cnt(20)='1' then
        pulse_cnt <=(others=>'0');
      else
        pulse_cnt <= pulse_cnt+'1';
      end if;
    end if;
  end process;

  -- Needed for KC705 FPGA
   process(clk40m)
   begin
     if clk40m'event and clk40m='1' then
       if pulse_lg ='1' then
          SOFT_TXRST_GT_r <= SOFT_TXRST_GT;
          SOFT_TXRST_GT_2r <= SOFT_TXRST_GT_r;
          SOFT_TXRST_GT_3r <= SOFT_TXRST_GT_2r;
          SOFT_TXRST_GT_4r <= SOFT_TXRST_GT_3r;
          SOFT_TXRST_GT_5r <= SOFT_TXRST_GT_4r;
          SOFT_TXRST_GT_6r <= SOFT_TXRST_GT_5r;
          SOFT_TXRST_GT_7r <= SOFT_TXRST_GT_6r;
          SOFT_TXRST_GT_8r <= SOFT_TXRST_GT_7r;
          SOFT_TXRST_GT_9r <= SOFT_TXRST_GT_8r;

       end if;
     end if;
   end process;


rxalign_auto : for i in 0 downto 0 generate

    auto_rxrst : entity work.FELIX_GBT_RX_AUTO_RST
      port map
      (
        FSM_CLK                 => clk40m,
        pulse_lg                => pulse_lg,
        GTHRXRESET_DONE         => gth_rx_rst_done(i) and rxfsmresetdone_out(i),
        alignment_chk_rst       => alignment_chk_rst_c1(i),
        GBT_LOCK                => alignment_done(i),
        AUTO_GTH_RXRST          => auto_gth_rxrst(i),
        ext_trig_realign        => ext_trig_realign(i),
        AUTO_GBT_RXRST          => auto_gbt_rxrst(i)
        );




    rafsm : entity work.FELIX_GBT_RXSLIDE_FSM
      port map
      (
        ext_trig_realign        => ext_trig_realign(i),

        FSM_RST                 => FSM_RST(i),
        FSM_CLK                 => clk40m,

        GBT_LOCK                => alignment_done(i),
        RxSlide                 => RxSlide_c(i),

        alignment_chk_rst       => alignment_chk_rst_c(i)
        );

    FSM_RST(i) <= RX_RESET_i(i);


    alignment_chk_rst_f(i) <= alignment_chk_rst(i) or alignment_chk_rst_c(i) or alignment_chk_rst_c1(i);


  end generate;



sma_p <= TX_FLAG;
sma_n <= RX_FLAG;




process(clk40m)
begin
if clk40m'event and clk40m='1' then

if txcnt_p(0)="00000000" then
 io_p_p2 <='1';
 else
 io_p_p2 <='0';
 end if;
 io_p_p1 <= io_p_p2;
 io_p <= io_p_p1;
end if;
end process;


--process(gt0_rxusrclk)
--begin
--if gt0_rxusrclk'event and gt0_rxusrclk='1' then
--RX_FLAG_r <= RX_FLAG;
--if RX_FLAG_r='1'  and  RX_120b_out(0)(111 downto 104)="00000000" then
-- io_n <='1';
-- else
-- io_n <='0';
-- end if;

--end if;
--end process;

process(RX_FRAME_CLK)
begin
if RX_FRAME_CLK'event and RX_FRAME_CLK='1' then
if  RX_120b_out(0)(111 downto 104)="00000000" then
 io_n <='1';
 else
 io_n <='0';
end if;
end if;
end process;

GBT_gen : for i in 0 downto 0 generate

 process(gt0_rxusrclk)
  begin
    if gt0_rxusrclk'event and gt0_rxusrclk='1' then
      BITSLIP_MANUAL_r(i) <= RxSlide_i(i) or RxSlide_c(i);
      BITSLIP_MANUAL_2r(i)  <= BITSLIP_MANUAL_r(i);
      RxSlide(i) <= BITSLIP_MANUAL_r(i) and (not BITSLIP_MANUAL_2r(i));
    end if;
  end process;

GBT_inst: entity work.gbtTxRx_FELIX
  generic map
    (
      channel                   => i
      )
  Port map
    (
           error_o                 => error_fec(i),
           RX_FLAG                 => RX_FLAG,--RX_FLAG_O(i),
           TX_FLAG                 => TX_FLAG,--TX_FLAG_O(i),

           Tx_DATA_FORMAT          => DATA_MODE_i(1 downto 0),--"00",--DATA_TXFORMAT_i(2*i+1 downto 2*i),
           Rx_DATA_FORMAT          => DATA_MODE_i(3 downto 2),--"00",--DATA_RXFORMAT_i(2*i+1 downto 2*i),

           Tx_latopt_tc            => '1',--TX_OPT(i),
           Tx_latopt_scr           => '1',--TX_OPT(24+i),
           RX_LATOPT_DES           => '1',--RX_OPT(i),

           TX_TC_METHOD            => TX_TC_METHOD,--one time(reset) phase checking, or cycle by cycle checking
           TC_EDGE                 => TC_EDGE,
           TX_TC_DLY_VALUE         => TX_TC_DLY_VALUE(4*i+2 downto 4*i),

           alignment_chk_rst       => alignment_chk_rst_f(i),
           alignment_done_O        => alignment_done(i),
           L40M                    => clk40m,
           outsel_i                => '0',--outsel_ii(i),
           outsel_o                => open,--outsel_o(i),

        --   BITSLIP_MANUAL          => RxSlide_i(i) or RxSlide_c(i),
        --   BITSLIP_SEL             => '0',--RxSlide_Sel(i),
        --   GT_RXSLIDE              => RxSlide(i),

           OddEven                 => '0',--OddEven_i(i),
           TopBot                  => '0',--TopBot_i(i),
           data_sel                => "0000",--data_sel(4*i+3 downto 4*i),

           TX_RESET_I              => TX_RESET_f(i),
           TX_FRAMECLK_I           => clk40m,--TX_FRAME_CLK_I(i),
           TX_WORDCLK_I            => GT_TX_WORD_CLK(i),
          -- TX_ISDATA_SEL_I    => TX_IS_DATA(i),
           TX_DATA_120b_I          => x"59" & txcnt(i) & txcnt(i) & txcnt(i) & txcnt(i) & txcnt(i) & txcnt(i) & txcnt(i) & txcnt(i) & txcnt(i) & txcnt(i) & x"00000000",--TX_120b_in(i),
           TX_DATA_20b_O           => TX_DATA_20b(i),

           RX_RESET_I              => RX_RESET_i(i) or auto_gbt_rxrst(i),
           RX_FRAME_CLK_O          => RX_FRAME_CLK,
           RX_WORD_IS_HEADER_O     => open,
           RX_HEADER_FOUND         => open,
           RX_ISDATA_FLAG_O        => open,
           RX_DATA_20b_I           => RX_DATA_20b_i(i),
           RX_DATA_120b_O          => RX_120b_out(i),
           des_rxusrclk            => gt0_rxusrclk,
           RX_WORDCLK_I            => gt0_rxusrclk
      );

      process(gt0_rxusrclk)
      begin
          if gt0_rxusrclk'event and gt0_rxusrclk='1' then
              RX_DATA_20b_r(i) <= RX_DATA_20b(i);
          end if;
      end process;

      RX_DATA_20b_i(i) <= RX_DATA_20b(i) when oddeven(i)='0' else
          RX_DATA_20b_r(i)(0) & RX_DATA_20b(i)(19 downto 1);

TX_RESET_f(i) <= TX_RESET_i(i) or (not mmcm_lck);



process(error_clr,clk40m)
begin
if error_clr='1' then

errors(i) <='0';
errcnt(i) <="000000";
elsif clk40m'event and clk40m='1' then
  txcnt_p(i) <=txcnt_p(i) + '1';
  txcnt_p1(i) <=txcnt_p(i);
  txcnt(i) <=txcnt_p1(i);
  if RX_120b_out(i)(119 downto 112) = x"59"  and RX_120b_out(i)(111 downto 72)=RX_120b_out(i)(71 downto 32) then
    errors(i)<=errors(i);
    errcnt(i) <= errcnt(i);
  else
    errors(i)<='1';
    errcnt(i) <= errcnt(i)+'1';
 end if;
end if;

end process;


end generate;


vio_inst: vio_1
PORT MAP(
clk => clk10m,
probe_in0 => alignment_done,
probe_in1 =>  errors,
probe_in2 => gth_cdr_stable,
probe_in3 => gth_tx_rst_done,
probe_in4 => gth_rx_rst_done,
probe_in5 => error_fec,

probe_in6 => errcnt(0),
probe_in7 => errcnt(1),
probe_in8 => errcnt(2),
probe_in9 => errcnt(3),
probe_in10 => (others=>mmcm_lck),
probe_in11 => (others=>mmcm_lck),

probe_out0 => RxSlide_i,
probe_out1 => DATA_MODE_i,
probe_out2 => TX_RESET_i,
probe_out3 => RX_RESET_i,
probe_out4 => alignment_chk_rst,
probe_out5 => TX_TC_METHOD_vec,
probe_out6 => TX_TC_DLY_VALUE,
probe_out7 => error_clr_vec,
probe_out8 => gth_tx_pll_rst,
probe_out9 => oddeven,
probe_out10 => gth_rx_rst,
probe_out11 => gth_tx_rst,
probe_out12 => gth_rst,
probe_out13 => rstt_vec,
probe_out14 => SOFT_RXRST_ALL,
probe_out15 => SOFT_TXRST_ALL,
probe_out16 => soft_reset_in,
probe_out17 => gttxreset,
probe_out18 => SOFT_RXRST_GT,
probe_out19 => SOFT_TXRST_GT,
probe_out20 => TC_EDGE_vec
);



TC_EDGE <= TC_EDGE_vec(0);
error_clr    <= error_clr_vec(0);
TX_TC_METHOD <= TX_TC_METHOD_vec(0);
rstt         <= rstt_vec(0);

rx_usrclk_bufg1: bufg
  port map
  (
    I => rxoutclk(0),
    O => gt0_rxusrclk

  );


gth_rx_rst_f(0) <= auto_gth_rxrst(0) or gth_rx_rst(0);

gth_rst_f(0)    <= gth_rst(0) or (not mmcm_lck);
gth_tx_rst_f(0) <= gth_tx_rst(0) or (not mmcm_lck);


---------
--Note, when using TXOUTCLK to generate TXUSRCLK,
-- To get a fixed phase between the TTC 40M to the 240MHz TXUSRCLK
-- TXOUTCLK (which is MGTREFCLK) must be 240M/120M/80M....

-- If TXOUTCLK(which is MGTREFCLK) is 160MHz...Or other bad frequencies
-- We should use the TTC 40M to generate 240M clock by MMCM see details in below segment

---------

TXUSRCLK_GEN_240: if RefClk = 240 generate
tx_usrclk_bufg1: bufg
  port map
  (
    I => TXOUTCLK(0),
    O =>GT_TX_WORD_CLK(0)
  );

end generate;

TXUSRCLK_GEN_160: if RefClk = 160 generate

-- The input 40M should be cleaned, and BUFG USED
-- When using this method, the MGTREFCLK 160M should be synchronized with TTC 40M
-- The generated 240MHz clock must have at least two selectable phases since
-- the phase between it and the internal 240MHz clock is uncertain
-- Here we simply put a mmcm here, to really use this case,
-- This part must be changed

tx_usrclk_bufg1:  clk_wiz_7
 port map
 (
    clk_in1 => SYSTEM_TTC_40M,

    clk_out1 => GT_TX_WORD_CLK(0),
    reset => '0',
    locked => open
 );
end generate;



TXUSRCLK_GEN_120: if RefClk = 120 generate
tx_usrclk_bufg1:  clk_wiz_6
 port map
 (
    clk_in1 => TXOUTCLK(0),
    clk_out1 => GT_TX_WORD_CLK(0),
    reset => '0',
    locked => open
 );
end generate;


GT_gen : for i in 0 downto 0 generate

  GT_inst:entity work.GT_CPLL_Wrapper
    generic map
    (
    DIVIDER_TOP =>  (480/RefClk)
    )
    port map
    (
      GT_RefClk => gtrefclk0_in,--local_rx_240m_in,--gtrefclk0_in,
      DRP_CLK_IN => clk40m,

      --- RX clock, for each channel
      gt0_rxusrclk_in  => gt0_rxusrclk,--(i)
      gt0_rxoutclk_out => rxoutclk(i),
      gt0_txusrclk_in  => GT_TX_WORD_CLK(i),
      gt0_txoutclk_out => TXOUTCLK(i),


-----------------------------------------
---- STATUS signals
-----------------------------------------

      gt0_txresetdone_out  => gth_tx_rst_done(i),--: out  std_logic_vector(3 downto 0);
      gt0_rxresetdone_out  => gth_rx_rst_done(i),--: out  std_logic_vector(3 downto 0);

      gt0_tx_fsm_reset_done_out => txfsmresetdone_out(i),--: out  std_logic_vector(3 downto 0);
      gt0_rx_fsm_reset_done_out => rxfsmresetdone_out(i),--: out  std_logic_vector(3 downto 0);

      gt0_cpllfbclklost_out        => open,--  : out  std_logic_vector(3 downto 0);
      gt0_cplllock_out             => open,--   : out  std_logic_vector(3 downto 0);

      gt0_rxcdrlock_out        =>   gth_cdr_stable(i),-- : out  std_logic_vector(3 downto 0);
   -- gt0_qplllock_out         => open,--      : out  std_logic;
---------------------------
---- CTRL signals
---------------------------
      gt0_rxslide_in               => RxSlide(i),
      gt0_txuserrdy_in             => '1',
      gt0_rxuserrdy_in             => '1',


----------------------------------------------------------------
----------RESET SIGNALs
----------------------------------------------------------------

      SOFT_RESET_IN                =>'0',
      GTTX_RESET_IN                => gth_tx_rst_f(i),
      GTRX_RESET_IN                => gth_rx_rst_f(i),
      gt0_cpllreset_in             => gth_tx_pll_rst(i) or SOFT_TXRST_GT_9r(i),
      -- for other FPGA,"or SOFT_TXRST_GT_9r(i)" may be not needed, like ZC706
      -- for KC705 is needed

      SOFT_TXRST_GT     => SOFT_TXRST_GT(i),
      SOFT_RXRST_GT     => SOFT_RXRST_GT(i),
    --SOFT_TXRST_ALL      : in   std_logic;
    --SOFT_RXRST_ALL      : in   std_logic;

-----------------------------------------------------------
----------- Data and TX/RX Ports
-----------------------------------------------------------

      gt0_txdata_in  => TX_DATA_20b(i),
      gt0_rxdata_out => RX_DATA_20b(i),


      gt0_gthrxn_in                                =>  gthrxn_in(i),
      gt0_gthrxp_in                                =>  gthrxp_in(i),
      gt0_gthtxn_out                               =>  gthtxn_out(i),
      gt0_gthtxp_out                               =>  gthtxp_out(i)

  );


end generate;


end Behavioral;
