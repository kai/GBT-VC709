# GBT-VC709

- The GBT part is the same with KC705 example. 
- The only difference is transceiver IP. 
- So please copy the GBT directory from https://github.com/simpway/GBT_KC705.git

## Note
- The latency (due to GTH RX side) is about 2.6 ns bigger than KC705.

- See https://github.com/simpway/GBT_KC705 for the latency value, and the usage. Especially the ***Caution*** part.
